package gui

import (
	"image"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/mnlphlp/tile_image_generator/data"
	"gitlab.com/mnlphlp/tile_image_generator/data/scaleOptions"
)

type GUI struct {
	guiImg   *canvas.Image
	progress binding.ExternalFloat
	savePath binding.String
	window   fyne.Window
}

func NewGUI(w fyne.Window) *GUI {
	return &GUI{
		savePath: binding.NewString(),
		window:   w,
	}
}

func (g *GUI) MakeUI(settings *data.Settings, img image.Image, progress *float64, run func()) fyne.CanvasObject {
	// set object properties
	g.guiImg = canvas.NewImageFromImage(img)
	g.guiImg.FillMode = canvas.ImageFillContain
	g.progress = binding.BindFloat(progress)
	// create objects
	// size
	sizeBinding := binding.BindInt(&settings.Size)
	sizeSlider := widget.NewSlider(5, 100)
	sizeSlider.SetValue(float64(settings.Size))
	sizeSlider.OnChanged = func(v float64) { sizeBinding.Set(int(v)) }
	// scale
	scaleSelect := widget.NewSelect(scaleOptions.All, func(s string) {
		settings.Scale = s
	})
	scaleSelect.Selected = settings.Scale
	// tiles
	tiles := binding.BindString(&settings.TilesPath)
	// construct the GUI
	settingsList := container.NewVBox(
		container.NewCenter(widget.NewLabel("Tile Image Generator")),
		layout.NewSpacer(),
		container.NewGridWithColumns(2,
			// size
			widget.NewLabelWithData(binding.IntToStringWithFormat(sizeBinding, "Size (%d):")),
			sizeSlider,
			// scale
			widget.NewLabel("Scale-Algorithm:"),
			scaleSelect,
			// interval
			widget.NewLabel("Interval between steps in ms:"),
			widget.NewEntryWithData(binding.IntToString(binding.BindInt(&settings.Interval))),
			// seed
			widget.NewLabel("Seed:"),
			widget.NewEntryWithData(binding.BindString(&settings.Seed)),
			// parallel
			widget.NewLabel("Parallel:"),
			widget.NewCheckWithData("", binding.BindBool(&settings.Parallel)),
		),
		container.NewGridWithColumns(3,
			widget.NewLabel("Save image as:"),
			widget.NewEntryWithData(tiles),
			widget.NewButtonWithIcon("Open", theme.DocumentSaveIcon(), func() {
				d := dialog.NewFolderOpen(
					func(uri fyne.ListableURI, err error) {
						if uri == nil {
							return
						}
						tiles.Set(uri.Path())
					},
					g.window,
				)
				d.Show()
			}),
			widget.NewLabel("Save image:"),
			widget.NewEntryWithData(g.savePath),
			widget.NewButtonWithIcon("Save", theme.DocumentSaveIcon(), func() {
				data.SaveImage(img, must(g.savePath.Get()))
			}),
		),
		widget.NewButtonWithIcon("Generate", theme.ConfirmIcon(), run),
		layout.NewSpacer(),
	)
	return container.NewBorder(
		nil,
		widget.NewProgressBarWithData(g.progress),
		settingsList,
		nil,
		g.guiImg,
	)
}

func must[T any](val T, err error) T {
	if err != nil {
		panic(err)
	}
	return val
}

func (g *GUI) Refresh() {
	g.progress.Reload()
	g.guiImg.Refresh()
}
