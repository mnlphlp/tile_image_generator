package scaleOptions

const Nearest = "NearestNeighbor"
const Approx = "ApproxBilinear"
const Bilinear = "Bilinear"
const Catmull = "CatmullRom"

var All = []string{Nearest, Approx, Bilinear, Catmull}
