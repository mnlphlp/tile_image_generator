package data

import (
	"image"
	"image/png"
	"os"
	"sync"
)

type State struct {
	Tiles      []Tile
	Image      *image.NRGBA
	Grid       [][]*GridItem
	Done       bool
	Iteration  int
	Iterations int
	Progress   float64
	TileSize   int
	Running    sync.WaitGroup
	Settings
}

type Settings struct {
	AbortAfter int
	Seed       string
	Scale      string
	Interval   int
	Parallel   bool
	Size       int
	TilesPath  string
	OutDir     string
	ResultPath string
	NoGui      bool
}

type Tile struct {
	Img   image.Image
	Edges [4]int
}

type GridItem struct {
	Collapsed     bool
	Drawn         bool
	PossibleTiles []*Tile
	X             int
	Y             int
}

type TileConfig struct {
	ImgPath string
	Edges   [4]int
	Mirror  bool
	Rotate  bool
}

func SaveImage(img image.Image, path string) {
	if path == "" || img == nil {
		return
	}
	out, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	png.Encode(out, img)
}
