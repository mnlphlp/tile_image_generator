package main

import (
	"crypto/sha1"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"image"
	"image/draw"
	"math"
	"math/rand"
	"os"
	"path"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"github.com/disintegration/imaging"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mnlphlp/tile_image_generator/data"
	"gitlab.com/mnlphlp/tile_image_generator/data/scaleOptions"
	"gitlab.com/mnlphlp/tile_image_generator/gui"

	drawx "golang.org/x/image/draw"
)

func main() {
	s := data.Settings{}
	setup(&s)
	if state.NoGui {
		run(s)
		return
	}
	a := app.New()
	w := a.NewWindow("Tile Image Generator")
	ui = gui.NewGUI(w)
	w.Resize(fyne.NewSize(float32(windowSizeDefault), float32(windowSizeDefault)))
	w.SetContent(ui.MakeUI(&s, state.Image, &state.Progress, func() { runFromUi(s) }))
	w.ShowAndRun()
}

const spacing = 0

const windowSizeDefault = 800

const (
	UP = iota
	RIGHT
	DOWN
	LEFT
)

var state = data.State{}
var ui *gui.GUI
var lastTilesKey string

func runFromUi(s data.Settings) {
	// stop last run
	state.Done = true
	state.Running.Wait()
	state.Running.Add(1)
	go func() {
		defer state.Running.Done()
		reset(s)
		run(s)
	}()
}

func getConfig(p string) ([]data.TileConfig, int) {
	jsonData, err := os.ReadFile(path.Join(p, "config.json"))
	if err != nil {
		panic(err)
	}
	var config []data.TileConfig
	json.Unmarshal(jsonData, &config)
	return config, len(config)
}

func getImageFromFilePath(filePath string, scale string, size int) image.Image {
	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	img, _, err := image.Decode(f)
	if err != nil {
		panic(err)
	}
	scaled := image.NewNRGBA(image.Rect(0, 0, size, size))
	switch scale {
	case scaleOptions.Nearest:
		drawx.NearestNeighbor.Scale(scaled, scaled.Bounds(), img, img.Bounds(), draw.Src, nil)
	case scaleOptions.Approx:
		drawx.ApproxBiLinear.Scale(scaled, scaled.Bounds(), img, img.Bounds(), draw.Src, nil)
	case scaleOptions.Bilinear:
		drawx.BiLinear.Scale(scaled, scaled.Bounds(), img, img.Bounds(), draw.Src, nil)
	case scaleOptions.Catmull:
		drawx.CatmullRom.Scale(scaled, scaled.Bounds(), img, img.Bounds(), draw.Src, nil)
	default:
		fmt.Printf("unknown scale method: %v\n  Supported Methods: %v", scale, strings.Join(scaleOptions.All, ", "))
		os.Exit(1)
	}
	return scaled
}

func setup(s *data.Settings) {
	flag.StringVar(&s.Seed, "seed", "", "seed for random number generator (\"\" => random seed)")
	flag.IntVar(&s.Interval, "interval", 0, "interval between steps in ms")
	flag.IntVar(&s.Size, "size", 5, "size of the grid")
	flag.StringVar(&s.OutDir, "animation", "", "if given save image for each step to this directory")
	flag.StringVar(&s.ResultPath, "result", "", "if given save resulting image to this path")
	flag.BoolVar(&s.NoGui, "no-gui", false, "if given do not show image during generation")
	flag.IntVar(&s.AbortAfter, "abort-after", 0, "if given abort after given number of iterations")
	flag.StringVar(&s.Scale, "scale", scaleOptions.Nearest, fmt.Sprintf("scale method for tiles (%s)", strings.Join(scaleOptions.All, ", ")))
	flag.StringVar(&s.TilesPath, "tiles", "tiles", "path to tiles directory")
	flag.BoolVar(&s.Parallel, "parallel", false, "if given use parallel algorithm")
	flag.Parse()
	reset(*s)
}

func collapse() {
	if state.Parallel {
		collapseParallel()
	} else {
		collapseSequential()
	}
}

func collapseParallel() {
	wg := sync.WaitGroup{}
	wg.Add(state.Size)
	collapse := int32(0)
	minCount := int32(0)
	atomic.StoreInt32(&minCount, int32(len(state.Tiles)+1))
	for x := 0; x < state.Size; x++ {
		go func(x int) {
			defer wg.Done()
			for y := 0; y < state.Size; y++ {
				// skip already collapsed tiles
				if state.Grid[x][y].Collapsed {
					continue
				}
				// update possible tiles
				updatePossibleTiles(x, y)
				optCount := len(state.Grid[x][y].PossibleTiles)
				oldCount := minCount
				if optCount < int(oldCount) {
					atomic.CompareAndSwapInt32(&minCount, oldCount, int32(optCount))
				}
				if optCount == 1 && atomic.CompareAndSwapInt32(&collapse, 0, 1) {
					state.Grid[x][y].Collapsed = true
					return
				}
			}
		}(x)
	}
	wg.Wait()
	// if no item collapsed, pick one from items with least options
	if collapse == 1 {
		return
	}
	minItems := getMinItems(int(minCount))
	if len(minItems) == 0 {
		state.Done = true
		return
	}
	collapseItem(minItems[rand.Intn(len(minItems))])
}

func collapseSequential() {
	minCount := len(state.Tiles) + 1
	for x := 0; x < state.Size; x++ {
		for y := 0; y < state.Size; y++ {
			// skip already collapsed tiles
			if state.Grid[x][y].Collapsed {
				continue
			}
			// update possible tiles
			updatePossibleTiles(x, y)
			optCount := len(state.Grid[x][y].PossibleTiles)
			if optCount < minCount && optCount > 0 {
				minCount = optCount
			}
			if optCount == 1 {
				state.Grid[x][y].Collapsed = true
				return
			}
		}
	}
	// if no item collapsed, pick one from items with least options
	minItems := getMinItems(minCount)
	if len(minItems) == 0 {
		state.Done = true
		return
	}
	collapseItem(minItems[rand.Intn(len(minItems))])
}

func collapseItem(item *data.GridItem) {
	item.Collapsed = true
	posCount := len(item.PossibleTiles)
	posTiles := item.PossibleTiles
	rand.Shuffle(posCount, func(a, b int) {
		posTiles[a], posTiles[b] = posTiles[b], posTiles[a]
	})
	for _, tile := range posTiles {
		item.PossibleTiles = []*data.Tile{tile}
		ok := true
		// test if this makes any surrounding tiles impossible to collapse
	pos:
		for x := item.X - 1; x <= item.X+1; x++ {
			for y := item.Y - 1; y <= item.Y+1; y++ {
				if x < 0 || x >= state.Size || y < 0 || y >= state.Size {
					// ignore outside of grid and current item
					continue
				}
				if state.Grid[x][y].Collapsed {
					// ignore already collapsed items
					continue
				}
				tileOk := false
				// check if there are any options remaining
				for _, t := range state.Grid[x][y].PossibleTiles {
					if x < item.X && t.Edges[RIGHT] == item.PossibleTiles[0].Edges[LEFT] ||
						x > item.X && t.Edges[LEFT] == item.PossibleTiles[0].Edges[RIGHT] ||
						y < item.Y && t.Edges[DOWN] == item.PossibleTiles[0].Edges[UP] ||
						y > item.Y && t.Edges[UP] == item.PossibleTiles[0].Edges[DOWN] {
						tileOk = true
						break
					}
				}
				if !tileOk {
					ok = false
					break pos
				}
			}
		}
		if ok {
			return
		}
	}
	// if we get here, no tile was ok, so just pick one
	log.Warn("no tile was ok, picking random one")
	item.PossibleTiles = []*data.Tile{posTiles[rand.Intn(posCount)]}
}

func getMinItems(minCount int) []*data.GridItem {
	var minItems []*data.GridItem
	for x := 0; x < state.Size; x++ {
		for y := 0; y < state.Size; y++ {
			// skip collapsed items
			if state.Grid[x][y].Collapsed {
				continue
			}
			optCount := len(state.Grid[x][y].PossibleTiles)
			if optCount == minCount {
				// add all tiles with the minimum option count
				minItems = append(minItems, state.Grid[x][y])
			}
		}
	}
	return minItems
}

func updatePossibleTiles(x, y int) {
	item := state.Grid[x][y]
	possibleTiles := make([]*data.Tile, 0)
	for _, tile := range item.PossibleTiles {
		if isPossibleTile(x, y, tile) {
			possibleTiles = append(possibleTiles, tile)
		}
	}
	if len(possibleTiles) == 0 {
		fmt.Println("no possible tiles for item at ", x, y)
		item.Drawn = true
		item.Collapsed = true
	}
	item.PossibleTiles = possibleTiles
}

func isPossibleTile(x, y int, tile *data.Tile) bool {
	match := 0
	// match top
	if y > 0 {
		for _, nextTile := range state.Grid[x][y-1].PossibleTiles {
			if nextTile.Edges[DOWN] == tile.Edges[UP] {
				match++
				break
			}
		}
	} else {
		match++
	}

	// match right
	if x < state.Size-1 {
		for _, nextTile := range state.Grid[x+1][y].PossibleTiles {
			if nextTile.Edges[LEFT] == tile.Edges[RIGHT] {
				match++
				break
			}
		}
	} else {
		match++
	}

	// match bottom
	if y < state.Size-1 {
		for _, nextTile := range state.Grid[x][y+1].PossibleTiles {
			if nextTile.Edges[UP] == tile.Edges[DOWN] {
				match++
				break
			}
		}
	} else {
		match++
	}

	// match left
	if x > 0 {
		for _, nextTile := range state.Grid[x-1][y].PossibleTiles {
			if nextTile.Edges[RIGHT] == tile.Edges[LEFT] {
				match++
				break
			}
		}
	} else {
		match++
	}

	//return match == 4
	if match == 4 {
		return true
	}
	return false
}

func run(s data.Settings) {
	if state.Interval == 0 {
		start := time.Now()
		for !state.Done {
			update()
		}
		fmt.Printf("done in %d ms\n", time.Since(start).Milliseconds())
		return
	}
	ticker := time.NewTicker(time.Duration(state.Interval) * time.Millisecond)
	for {
		<-ticker.C
		if state.Done {
			return
		}
		update()
	}
}

func reset(settings data.Settings) {
	state.Done = false
	state.Iteration = 0
	state.Settings = settings
	state.Iterations = state.Size * state.Size

	if state.Seed == "" {
		rand.Seed(time.Now().UnixNano())
	} else {
		h := sha1.New()
		h.Write([]byte(state.Seed))
		fmt.Println(h.Sum(nil))
		hash := h.Sum(nil)
		var seed int64
		seed += int64(binary.BigEndian.Uint64(hash[0:8]))
		seed += int64(binary.BigEndian.Uint64(hash[8:16]))
		seed += int64(binary.BigEndian.Uint64(append(hash[16:20], 0, 0, 0, 0)))
		rand.Seed(seed)
	}

	fmt.Printf("size: %v\n", state.Size)
	fmt.Printf("seed: %v\n", state.Seed)
	fmt.Printf("interval: %v\n", state.Interval)
	// load tiles
	state.TileSize = int(math.Round(float64(windowSizeDefault) / float64(state.Size)))
	tilesKey := fmt.Sprintf("%s%d%s", state.TilesPath, state.TileSize, state.Scale)
	if lastTilesKey != tilesKey {
		lastTilesKey = tilesKey
		state.Tiles = loadTiles(state.TileSize)
	}
	// create/clear image
	windowSize := state.TileSize * state.Size
	if state.Image == nil {
		state.Image = image.NewNRGBA(image.Rect(0, 0, windowSize, windowSize))
	} else {
		draw.Draw(state.Image, state.Image.Bounds(), image.Transparent, image.Point{}, draw.Src)
	}
	// create grid
	state.Grid = make([][]*data.GridItem, state.Size)
	for x := 0; x < state.Size; x++ {
		state.Grid[x] = make([]*data.GridItem, state.Size)
		for y := 0; y < state.Size; y++ {
			itemTiles := make([]*data.Tile, len(state.Tiles))
			for i := 0; i < len(state.Tiles); i++ {
				itemTiles[i] = &state.Tiles[i]
			}
			state.Grid[x][y] = &data.GridItem{Collapsed: false, Drawn: false, PossibleTiles: itemTiles, X: x, Y: y}
		}
	}
}

func loadTiles(size int) []data.Tile {
	fmt.Println("loading tiles")
	config, tileCount := getConfig(state.TilesPath)
	tiles := make([]data.Tile, 0)
	for i := 0; i < tileCount; i++ {
		img := getImageFromFilePath(path.Join(state.TilesPath, config[i].ImgPath), state.Scale, size)
		t := data.Tile{Img: img, Edges: config[i].Edges}
		tiles = append(tiles, t)
		if config[i].Rotate {
			tiles = append(tiles, getRotated(t)...)
		}
	}
	fmt.Printf("loaded %d tiles\n", len(tiles))
	return tiles
}

func getRotated(t data.Tile) []data.Tile {
	// create rotated version of the tile
	rotated := make([]data.Tile, 3)
	rotated[0] = data.Tile{
		Img:   imaging.Rotate90(t.Img),
		Edges: [4]int{t.Edges[RIGHT], t.Edges[DOWN], t.Edges[LEFT], t.Edges[UP]},
	}
	rotated[1] = data.Tile{
		Img:   imaging.Rotate180(t.Img),
		Edges: [4]int{t.Edges[DOWN], t.Edges[LEFT], t.Edges[UP], t.Edges[RIGHT]},
	}
	rotated[2] = data.Tile{
		Img:   imaging.Rotate270(t.Img),
		Edges: [4]int{t.Edges[LEFT], t.Edges[UP], t.Edges[RIGHT], t.Edges[DOWN]},
	}
	return rotated
}

func update() {
	state.Iteration++
	collapse()
	updateImage()
	if math.Mod(float64(state.Iteration), 100) == 0 {
		//fmt.Printf("iteration: %v/%v\n", state.Iteration, state.Iterations)
	}
	if state.AbortAfter > 0 && state.Iteration >= state.AbortAfter {
		state.Done = true
	}
	if state.Done && state.ResultPath != "" {
		data.SaveImage(state.Image, state.ResultPath)
	}
}

func updateImage() {
	drawImage()
	if ui != nil {
		if state.Done {
			state.Progress = 1
		} else {
			state.Progress = float64(state.Iteration) / float64(state.Iterations)
		}
		ui.Refresh()
	}
}

func drawImage() *image.NRGBA {
	ts := state.TileSize
	for x, row := range state.Grid {
		for y, item := range row {
			if item.Drawn {
				continue
			}
			tileImg := getImage(item)
			if tileImg == nil {
				continue
			}
			offsetX := int(math.Round(float64(ts*x + spacing/2)))
			offsetY := int(math.Round(float64(ts*y + spacing/2)))
			draw.Draw(state.Image, image.Rect(offsetX, offsetY, offsetX+ts, offsetY+ts), tileImg, image.Point{}, draw.Src)
			item.Drawn = true
		}
	}
	if state.OutDir != "" {
		outFile := fmt.Sprintf("%s/frame%03d.png", state.OutDir, state.Iteration)
		data.SaveImage(state.Image, outFile)
	}
	return state.Image
}

func getImage(item *data.GridItem) image.Image {
	if item.Collapsed {
		return item.PossibleTiles[0].Img
	}
	return nil
}
